import "./SeasonDisplay.css";
import React from "react";

const seasonConfig = {
  summer: {
    text: "Let's hit the beach!",
    iconName: "sun",
  },
  autumn: {
    text: "Time to rake the leaves!",
    iconName: "leaf",
  },
  winter: {
    text: "Burr it is cold!",
    iconName: "snowflake",
  },
  spring: {
    text: "Look at all of the growing green!",
    iconName: "umbrella",
  },
};

const getSeason = (lat, month) => {
  if ((month >= 0 && month < 2) || month === 11) {
    return lat > 0 ? "winter" : "summer";
  } else if (month >= 2 && month < 5) {
    return lat > 0 ? "spring" : "autumn";
  } else if (month >= 5 && month < 8) {
    return lat > 0 ? "summer" : "winter";
  } else {
    return lat > 0 ? "autumn" : "spring";
  }
};

const SeasonDisplay = (props) => {
  const season = getSeason(props.lat, new Date().getMonth());
  const { text, iconName } = seasonConfig[season]; // {text, iconName}
  /* const seasonText =
    season === "winter"
      ? "Burr, it's chilly"
      : season === "spring"
      ? "Look at all of the growing green!"
      : season === "summer"
      ? "Let's hit the beach!"
      : "Time to rake the leaves!";
  const iconText =
    season === "winter"
      ? "snowflake"
      : season === "spring"
      ? "leaf"
      : season === "summer"
      ? "sun"
      : "Time to rake the leaves!"; */

  console.log(season);
  return (
    <div className={`season-display ${season}`}>
      <i className={`icon-left massive ${iconName} icon`} />
      <h1>{text}</h1>
      <i className={`icon-right massive ${iconName} icon`} />
    </div>
  );
};

export default SeasonDisplay;
