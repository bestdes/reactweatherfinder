import React from "react";
import ReactDOM from "react-dom";
import SeasonDisplay from "./SeasonDisplay";
import SpinLoader from "./spinLoader";

class App extends React.Component {
  //   constructor(props) {
  //     super(
  //       props
  //     ); /* super is a reference to the parent class's constructor
  //         and we are passing the props variable to that parent class constructor */

  //     // This is the only time we do direct assignment to this.state (initialization)
  //     this.state = { lat: null, errorMessage: "" };
  //   }

  state = { lat: null, errorMessage: "" };

  componentDidMount() {
    // Sits and waits for updates, this is the scope/function where data loading should be
    console.log("My component was rendered to the screen");
    window.navigator.geolocation.getCurrentPosition(
      (position) => this.setState({ lat: position.coords.latitude }),
      // Make sure to always handle your errors
      (err) => this.setState({ errorMessage: err.message })
    );
  }

  componentDidUpdate() {
    // Sits and waits until component not longer shown on screen
    /* This is a good place to do more data loading when state/props change */
    console.log("My component did update! Meaning it rendered again!");
  }

  renderContent() {
    if (this.state.errorMessage && !this.state.lat) {
      return <div>Error: {this.state.errorMessage}</div>;
    }
    if (!this.state.errorMessage && this.state.lat) {
      return <SeasonDisplay lat={this.state.lat} />;
    } else {
      return <SpinLoader message='Please allow location request' />;
    }
  }

  // React says that we have to define render!! This shows components on the screen
  render() {
    // The render method strictly only returns JSX
    return <div>{this.renderContent()}</div>;
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
