import React from "react";

const SpinLoader = (props) => {
  return (
    <div class='ui active dimmer'>
      <div class='ui big text loader'>{props.message}</div>
    </div>
  );
};

SpinLoader.defaultProps = {
  message: "Loading...",
};

export default SpinLoader;
